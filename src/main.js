import Vue from 'vue'
import '@/plugins/vuetify'
import App from '@/App.vue'
import router from '@/plugins/router'
import store from '@/plugins/store'
import '@/plugins/registerServiceWorker'
import '@/plugins/firebase'

Vue.config.productionTip = false

import { Firebase } from '@/plugins/firebase'

let app = ''

Firebase.auth().onAuthStateChanged((user) => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }

  if (user) {
    store.dispatch('user/openDBChannel')
  } else {
    store.dispatch('user/closeDBChannel', { clearModule: true })
  }
})

//DEV!!!
window.Firebase = Firebase
window.store = store