import * as Firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/functions'
import 'firebase/storage'

const config = {
    apiKey: 'AIzaSyBmMNpiu-yYicwqN9JUJc2XfHrwiEjztq4',
    authDomain: 'stbscout.firebaseapp.com',
    databaseURL: 'https://stbscout.firebaseio.com',
    projectId: 'stbscout',
    storageBucket: 'stbscout.appspot.com',
    messagingSenderId: '949544790200',
    appId: '1:949544790200:web:ba2323facec60db9'
}

const initFirebase = async () => {
    Firebase.initializeApp(config)
    await Firebase.firestore().enablePersistence()
}

export { Firebase, initFirebase }