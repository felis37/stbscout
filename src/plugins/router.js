import Vue from 'vue'
import Router from 'vue-router'

//Views

import List from '@/views/List.vue'

import HeroHome from '@/components/Heros/HeroHome.vue'
import HeroGroup from '@/components/Heros/HeroGroup.vue'

import Group from '@/views/Group.vue'
import Post from '@/views/Post.vue'
import Page from '@/views/Page.vue'

import ContentCreate from '@/components/Content/ContentCreate.vue'
import ContentView from '@/components/Content/ContentView.vue'
import ContentManage from '@/components/Content/ContentManage.vue'
import ContentSettings from '@/components/Content/ContentSettings.vue'

import SignIn from '@/views/SignIn.vue'

Vue.use(Router)

export default new Router({
  routes: [
    //Home
    {
      path: '/',
      component: List,
      children: [{
        path: '',
        name: 'home',
        component: HeroHome
      }]
    },

    //Groups
    {
      path: '/avdelningar',
      component: Group,
      children: [
        {
          path: '/hantera',
          name: 'groups/manage',
          component: ContentManage
        }
      ]
    },
    {
      path: '/avdelningar/:id',
      component: Group,
      children: [
        {
          path: '',
          component: List,
          children: [{
            path: '',
            name: 'group/view',
            component: HeroGroup
          }]
        },
        {
          path: '/inställningar',
          name: 'group/settings',
          component: ContentSettings
        }
      ],
    },

    //Posts
    {
      path: '/inlägg',
      component: Post,
      children: [
        {
          path: '/skapa',
          name: 'post/create',
          component: ContentCreate,
        }
      ]
    },
    {
      path: '/inlägg/:id',
      component: Post,
      children: [
        {
          path: '',
          name: 'post/view',
          component: ContentView
        },
        {
          path: '/inställningar',
          name: 'post/settings',
          component: ContentSettings
        }
      ]
    },

    //Pages
    {
      path: '/för-ledare/logga-in',
      name: 'sign-in',
      component: SignIn
    }
  ],

  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
