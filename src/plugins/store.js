import Vue from 'vue'
import Vuex from 'vuex'
import VuexEasyFirestore from 'vuex-easy-firestore'
Vue.use(Vuex)

import { Firebase, initFirebase } from '@/plugins/firebase'

const posts = {
  firestorePath: 'posts',
  firestoreRefType: 'collection',
  moduleName: 'posts',
  statePropName: 'data',
  namespaced: true
}

const featuredPosts = {
  firestorePath: 'posts',
  firestoreRefType: 'collection',
  moduleName: 'featuredPosts',
  statePropName: 'data',
  namespaced: true
}

const pages = {
  firestorePath: 'pages',
  firestoreRefType: 'collection',
  moduleName: 'pages',
  statePropName: 'data',
  namespaced: true
}

const groups = {
  firestorePath: 'groups',
  firestoreRefType: 'collection',
  moduleName: 'groups',
  statePropName: 'data',
  namespaced: true
}

const user = {
  firestorePath: 'users/{userId}',
  firestoreRefType: 'document',
  moduleName: 'user',
  statePropName: 'data',
  namespaced: true
}

const easyFirestore = VuexEasyFirestore(
  [
    posts,
    featuredPosts,
    pages,
    groups,
    user
  ], { 
    logging: true, //DEV
    FirebaseDependency: Firebase
  }
)

const storeData = {
  state: {
    textEdit: {
      mode: false,
      action: '',
      textHasChanged: false
    },
    pageSettings: {
      pageSettingsMode: false
    },
    groups: {},
    pages: {},
    posts: {},
    dbPosts: {
      allPostsLoaded: false,
      postsPerPagination: 20,
      pagination: 0
    },
    user: {}
  },
  getters: {
    getPage: state => id => {
      return state.pages.data[id]
    },
    group: state => id => {
      return state.groups.data[id]
    },
    groupsIds: state => {
      return Object.keys(state.groups.data)
    },
    groupsLength: (state, getters) => {
      return getters.groupsIds.length
    },
    groupsReference: state => {
      return Object.values(state.groups.data).sort((a, b) => { return a.sortingPriority - b.sortingPriority }).map(group => { return { id: group.id, title: group.title } })
    },
    page: state => id => {
      return state.pages.data[id]
    },
    pagesReference: state => {
      return Object.values(state.pages.data).sort((a, b) => { return a.sortingPriority - b.sortingPriority }).map(page => { return { id: page.id, title: page.title } })
    },
    getDbPosts: (state) => (group = null) => {
      let posts = Object.values(state.posts.data).sort((a, b) => b.postDate - a.postDate)
      
      if (group) {
        posts = posts.filter(post => post.groups.includes(group))
      }

      return posts
    },
    featuredPosts: state => {
      return Object.values(state.featuredPosts.data).sort((a, b) => b.postDate - a.postDate)
    },
    getUserAuth: state => {
      if (typeof state.user.data === 'object' && Object.entries(state.user.data).length) {
        return(true)
      } else {
        return(false)
      }
    },
    getGroupsFromScoutnetMatchTable: state => {
      let matchTable = Object.values(state.groups.data).map(group => { return { [group.scoutnetId]: group.id } })
      
      return Object.assign({}, ...matchTable)
    },
  },
  mutations: {
    dbPostsAllLoaded(state) {
      state.dbPosts.allPostsLoaded = true
    },
    dbPostsIncrementPagination(state) {
      state.dbPosts.pagination++
    },
    setTextEditMode(state, value) {
      state.textEdit.mode = value
    },
    setTextEditAction(state, value) {
      state.textEdit.action = value
    },
    setTextEditTextHasChanged(state, value) {
      state.textEdit.textHasChanged = value
    },
    setClearTextEdit(state) {
      state.textEdit = {
        mode: false,
        action: '',
        textHasChanged: false
      }
    }
  },
  actions: {
    async loadDbPosts({ state, commit, dispatch }) {
      let response = await dispatch('posts/fetchAndAdd', { orderBy: ['postDate', 'desc'], limit: state.dbPosts.postsPerPagination })
      
      commit('dbPostsIncrementPagination')

      if (response.done) {
        commit('dbPostsAllLoaded')
      }
    },
    async doGetPostData({ dispatch }, id) {
      return await dispatch('posts/fetchById', id)
    },
    async doSetPostData({ dispatch }, params) {
      await dispatch('posts/set', params)
    },
    async doSetPageData({ dispatch }, params) {
      await dispatch('pages/set', params)
    }
  },
  plugins: [
    easyFirestore,
  ]
}

const store = new Vuex.Store(storeData)

initFirebase()

store.dispatch('groups/openDBChannel')
store.dispatch('pages/openDBChannel')

export default store