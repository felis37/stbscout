import { Firebase } from '@/plugins/firebase'

const uploadImageReturnUrl = async (imageFile, imageName, routeName) => {
    const subDirectory = routeName.split('/')[0]
    
    const storageRef = Firebase.storage().ref()

    const imageRef = storageRef.child(`${subDirectory}/${imageName}/images`)

    const uploadSnapshot = await imageRef.putString(imageFile, 'data_url')

    return await uploadSnapshot.ref.getDownloadURL()
}

const uploadDocument = async (documentName, documentFile, groupName) => {

}

export { uploadImageReturnUrl, uploadDocument }