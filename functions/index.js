const functions = require('firebase-functions')
const admin = require('firebase-admin')
const algoliasearch = require('algoliasearch')
const axios = require('axios')

admin.initializeApp()

/*------------*/

const ALGOLIA_ID = functions.config().algolia.app_id
const ALGOLIA_ADMIN_KEY = functions.config().algolia.api_key
const ALGOLIA_INDEX_NAME = 'posts'

const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY)

exports.onPostCreated = functions.firestore.document('posts/{postId}').onCreate((snap, context) => {
    const post = snap.data()

    post.objectID = context.params.postId

    const index = client.initIndex(ALGOLIA_INDEX_NAME)
    return index.saveObject(post)
})

exports.onPostUpdated = functions.firestore.document('posts/{postId}').onUpdate((change, context) => {
    const post = change.after.data()

    post.objectID = context.params.postId

    const index = client.initIndex(ALGOLIA_INDEX_NAME)
    return index.saveObject(post)
})

exports.onPostDeleted = functions.firestore.document('posts/{postId}').onDelete((snap, context) => {
    const index = client.initIndex(ALGOLIA_INDEX_NAME)
    return index.deleteObject(context.params.postId)
})

/*------------*/

exports.getScoutnetUser = functions.https.onCall(async (data, context) => {  
    const SCOUTNET_MEMBER_LIST_API_KEY = functions.config().scoutnet.member_list_api_key
    const SCOUTNET_MEMBER_ID = functions.config().scoutnet.member_id
    
    const ssno = `${data.substr(0, 6)}-${data.substr(6)}`

    const requestUrl = `https://www.scoutnet.se/api/group/memberlist?id=${SCOUTNET_MEMBER_ID}&key=${SCOUTNET_MEMBER_LIST_API_KEY}`    

    let responseData

    try {
        let response = await axios.get(requestUrl)
        responseData = response.data.data
    } catch (err) {
        throw new functions.https.HttpsError('external-request-error', 'Request to fetch data from external API failed')
    }

    for (let i = 0; i < Object.keys(responseData).length; i++) {
        let member = responseData[Object.keys(responseData)[i]]

        if (member.ssno.value.substr(2) === ssno && (member.group_role || (member.unit_role && member.unit_role.raw_value === '3'))) {
            let groups = []

            if (member.unit_role) {
                groups.push(member.unit.raw_value)
            }

            let admin = false

            if (member.group_role) {
                let groupRoles = member.group_role.raw_value.split(',')

                if (groupRoles.includes('36') || groupRoles.includes('136')) {
                    admin = true
                }
            }

            let phoneNumber = member.contact_mobile_phone.value

            phoneNumber = phoneNumber.replace(/\D/g, '')
            phoneNumber = (phoneNumber.substr(0, 2) === '46') ? `+${phoneNumber}` : `+46${phoneNumber.substr(1)}`

            return { phoneNumber: phoneNumber, firstName: member.first_name.value, groups: groups, admin: admin }
        }
    }

    throw new functions.https.HttpsError('not-found', 'No applicable user with given SSNO found')
})

exports.setUserInfo = functions.https.onCall(async (data, context) => {
    let userInfo = data.userInfo
    const groupMatchingTable = data.groupMatchingTable

    userInfo.groups = userInfo.groups.map(group => ( groupMatchingTable[group] ))

    if (context.auth) {
        try {
            await admin.firestore().collection('users').doc(context.auth.uid).set(userInfo, { merge: true })
        } catch (err) {
            throw new functions.https.HttpsError('write-fail', 'The data write to Firestore failed')
        }
    } else {
        throw new functions.https.HttpsError('unauthorized', 'The function must be called while authenticated')
    }
})